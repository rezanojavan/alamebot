package ir.alame.telegrambot.commands;

import ir.alame.telegrambot.common.AbstractCommand;
import ir.alame.telegrambot.common.RequestContext;
import ir.alame.telegrambot.utility.RegisterCommandUtil;
import org.telegram.telegrambots.api.methods.ParseMode;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 * @author reza
 * @version 1.0
 * @since 26/09/2017
 */
public class GradeCommandImpl extends AbstractCommand<SendMessage> {

    /**
     * Singleton instance
     */
    private volatile static GradeCommandImpl _instance = new GradeCommandImpl();

    /**
     * Grade command string representation
     */
    public static final String GRADE_CMD = BUNDLE.getString("GRADE_CMD");

    /**
     * Grade command message
     */
    private static final String GRADE_MESSAGE = BUNDLE.getString("GRADE_MESSAGE");

    /**
     * private constructor
     */
    private GradeCommandImpl(){}

    /**
     * Get current instance of this class
     * @return StartCommandImpl
     */
    public static GradeCommandImpl getInstance() {
        return _instance;
    }

    /**
     * This method generate message for command
     *
     * @param ctx to generate message for command
     * @return PartialBotApiMethod
     */
    @Override
    protected SendMessage getMessage(RequestContext ctx) {
        return new SendMessage().setText(GRADE_MESSAGE).setParseMode(ParseMode.HTML);
    }

    /**
     * Get command string representation
     *
     * @return String
     */
    @Override
    protected String getCommandText() {
        return GRADE_CMD;
    }

    /**
     * Init command as needed
     */
    @Override
    protected void doInit() {
        GradeSelectCommandImpl primary = new GradeSelectCommandImpl();
        primary.setCommandText("ابتدایی");
        GradeSelectCommandImpl high1 = new GradeSelectCommandImpl();
        high1.setCommandText("متوسطه 1");
        GradeSelectCommandImpl high2 = new GradeSelectCommandImpl();
        high2.setCommandText("متوسطه 2");
        GradeSelectCommandImpl preuni = new GradeSelectCommandImpl();
        preuni.setCommandText("کنکوری");
        RegisterCommandUtil.registerCommand(this);
        this.buttons.add(primary);
        this.buttons.add(high1);
        this.buttons.add(high2);
        this.buttons.add(preuni);
        this.buttons.add(StartCommandImpl.getInstance());
    }

    /**
     * process user input then return appropriate message
     *
     * @param ctx to generate response
     * @return Generic Message Type
     */
    @Override
    protected SendMessage doUserInput(RequestContext ctx) {
        return null;
    }
}
