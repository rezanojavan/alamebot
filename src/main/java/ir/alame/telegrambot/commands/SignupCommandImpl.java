package ir.alame.telegrambot.commands;

import ir.alame.telegrambot.common.AbstractCommand;
import ir.alame.telegrambot.common.CommandExecutor;
import ir.alame.telegrambot.common.RequestContext;
import ir.alame.telegrambot.utility.RegisterCommandUtil;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Contact;

/**
 * Request Contact
 *
 * @author reza
 * @version 1.0
 * @since 23/08/2017
 */
public class SignupCommandImpl extends AbstractCommand<SendMessage> {

    /**
     * Sign up command string representation
     */
    public static final String SIGN_UP_CMD = BUNDLE.getString("SIGN_UP_CMD");
    /**
     * Sign up command message
     */
    private static final String SIGN_UP_MESSAGE = "SIGN_UP_MESSAGE";

    /**
     * Singleton instance
     */
    private volatile static SignupCommandImpl _instance = new SignupCommandImpl();
    /**
     * Is Contact asked
     */
    private Boolean contactAsked = Boolean.FALSE;


    /**
     * private constructor
     */
    private SignupCommandImpl() {
    }

    /**
     * This method generate message for command
     *
     * @param ctx to generate message for command
     * @return PartialBotApiMethod
     */
    @Override
    protected SendMessage getMessage(RequestContext ctx) {
        CommandExecutor.getNeedUserInput().put(ctx.getIncomeRequest().getMessage().getChatId().toString(), this);
        return new SendMessage().setText(BUNDLE.getString(SIGN_UP_MESSAGE));
    }

    /**
     * Get command string representation
     *
     * @return String
     */
    @Override
    protected String getCommandText() {
        return this.SIGN_UP_CMD;
    }

    /**
     * Init command as needed
     */
    @Override
    protected void doInit() {
        StartCommandImpl.getInstance().registerMe(this);
        RegisterCommandUtil.registerCommand(this);
        this.buttons.add(StartCommandImpl.getInstance());
    }

    /**
     * process user input then return appropriate message
     *
     * @param ctx to generate response
     * @return Generic Message Type
     */
    @Override
    protected SendMessage doUserInput(RequestContext ctx) {
        Contact cnt = ctx.getIncomeRequest().getMessage().getContact();
        System.out.println(String.format("%s %s %s", cnt.getFirstName(), cnt.getLastName(), cnt.getPhoneNumber()));
        contactAsked = Boolean.TRUE;
        this.buttons.add(GradeCommandImpl.getInstance());
        return new SendMessage().setText("با تشکر از عضویت شما");
    }

    /**
     * Request User Contact
     *
     * @return
     */
    protected Boolean askContact() {
        return !contactAsked;
    }
}
