package ir.alame.telegrambot.commands;

import ir.alame.telegrambot.common.AbstractCommand;
import ir.alame.telegrambot.common.RequestContext;
import ir.alame.telegrambot.utility.BotMessageUtil;
import ir.alame.telegrambot.utility.RegisterCommandUtil;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 * Send commands message to client who is chatting Bot.
 *
 * @author reza
 * @version 1.0
 * @since 13/08/2017
 */
public class StartCommandImpl extends AbstractCommand<SendMessage> {

    /**
     * Singleton instance
     */
    private volatile static StartCommandImpl _instance = new StartCommandImpl();

    /**
     * private constructor
     */
    private StartCommandImpl(){}

    /**
     * Get current instance of this class
     * @return StartCommandImpl
     */
    public static StartCommandImpl getInstance() {
        return _instance;
    }

    /**
     * Welcome message
     */
    private static final String WELCOME_MESSAGE = "WELCOME_MESSAGE";

    /**
     * Intro command string representation
     */
    public static final String START_CMD = "/start";

    /**
     * This method getMessage the commands command then send response on callback. In commands command it reads INTRO.MD file
     * from BOT folder where all bot's files will be placed. To get bot path, it reads BOT_FILE_PATH property in
     * Bot.properties file. INTRO.MD file contain introduction message in HTML format to show to clients at first.
     *
     * @param ctx      to getMessage commands command
     */
    @Override
    protected SendMessage getMessage(RequestContext ctx) {
        return BotMessageUtil.generateMessage(ctx, BUNDLE.getString(WELCOME_MESSAGE)).setChatId(
                        ctx.getIncomeRequest().getMessage().getChatId());
    }

    /**
     * Get command string representation
     *
     * @return "/start"
     */
    @Override
    protected String getCommandText() {
        return StartCommandImpl.START_CMD;
    }

    /**
     * Init command as needed. for example register itself on other commands
     */
    @Override
    protected void doInit() {
        RegisterCommandUtil.registerCommand(this);
    }

    /**
     * process user input then return appropriate message
     *
     * @param ctx to generate response
     * @return Generic Message Type
     */
    @Override
    protected SendMessage doUserInput(RequestContext ctx) {
        return null;
    }

}
