package ir.alame.telegrambot.commands;

import ir.alame.telegrambot.common.AbstractDynamicCommand;
import ir.alame.telegrambot.common.RequestContext;
import ir.alame.telegrambot.utility.RegisterCommandUtil;
import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 * @author reza
 * @version 1.0
 * @since 26/09/2017
 */
public class GradeSelectCommandImpl extends AbstractDynamicCommand<SendMessage> {


    /**
     * This method generate message for command
     *
     * @param ctx to generate message for command
     * @return PartialBotApiMethod
     */
    @Override
    protected SendMessage getMessage(RequestContext ctx) {
        System.out.println("memeber " + ctx.getIncomeRequest().getMessage().getChatId() + " select " + this.getCommandText());
        return new SendMessage().setText("با تشکر");
    }

    /**
     * Get command string representation
     *
     * @return String
     */
    @Override
    protected String getCommandText() {
        return this.commandText;
    }

    /**
     * Init command as needed
     */
    @Override
    protected void doInit() {
        RegisterCommandUtil.registerCommand(this);
        this.buttons.add(StartCommandImpl.getInstance());
        this.buttons.add(GradeCommandImpl.getInstance());

    }

    /**
     * process user input then return appropriate message
     *
     * @param ctx to generate response
     * @return Generic Message Type
     */
    @Override
    protected SendMessage doUserInput(RequestContext ctx) {
        return null;
    }
}
