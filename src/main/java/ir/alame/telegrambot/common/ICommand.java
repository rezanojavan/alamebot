package ir.alame.telegrambot.common;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;

/**
 * Command Interface
 * @author reza
 * @version 1.0
 * @since 15/08/2017
 */
public interface ICommand {

    /**
     * This method run command then send response on callback parameter. To run the command, It call an abstract method
     * named 'getMessage' which implemented by command implementors.
     *
     * @param ctx to run command
     * @param callback to send response
     */
    void run(RequestContext ctx, TelegramLongPollingBot callback);

    /**
     * Initiate command
     */
    void init();
}
