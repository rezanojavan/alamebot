package ir.alame.telegrambot.common;

/**
 * Singleton interface
 *
 * @author reza
 * @version 1.0
 * @since 16/08/2017
 */
public interface ISingleton {

    /**
     * Get instance method which return singleton instance
     * @return get singleton instance
     */
    Object getInstance();
}
