package ir.alame.telegrambot.common;

/**
 * Dynamic command which initiated in runtime
 *
 * @author reza
 * @version 1.0
 * @since 17/08/2017
 */
public abstract class AbstractDynamicCommand<MessageType> extends AbstractCommand<MessageType> {

    /**
     * Command text
     */
    protected String commandText;

    public void setCommandText(String commandText) {
        this.commandText = commandText;
    }
}
