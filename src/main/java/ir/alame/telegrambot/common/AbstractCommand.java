package ir.alame.telegrambot.common;

import ir.alame.telegrambot.TelegramBotApplication;
import ir.alame.telegrambot.utility.BotMessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Command abstract class which implement base functionality. All commands must implement singleton pattern.
 *
 * @author reza
 * @version 1.0
 * @since 13/08/2017
 */
public abstract class AbstractCommand<MessageType> implements ICommand {

    /**
     * Console logger
     */
    private static final Logger logger = LoggerFactory.getLogger("TelegramBot");

    /**
     * Resource bundle contain messages
     */
    protected static final ResourceBundle BUNDLE = ResourceBundle.getBundle("bundle");

    /**
     * No data to display bundle key
     */
    protected static final String NO_DATA_TO_DISPLAY = "NO_DATA_TO_DISPLAY";

    /**
     * Return list of command to show as button to user
     */
    protected List<AbstractCommand> buttons = new ArrayList<AbstractCommand>();
    /**
     * Is this command initiated?
     */
    private Boolean initiated = Boolean.FALSE;

    /**
     * Base constructor which register this command to Main class
     */
    public AbstractCommand() {
        TelegramBotApplication.registerCommand(this);
    }

    /**
     * This method run command then send response on callback parameter. To run the command, It call an abstract method
     * named 'getMessage' which implemented by command implementors.
     *
     * @param ctx      to run command
     * @param callback to send response
     */
    public void run(RequestContext ctx, TelegramLongPollingBot callback) {
        sendIt(ctx, callback, getMessage(ctx));
    }

    /**
     * This method send given message to telegram server
     * @param ctx to send message
     * @param callback to send message
     * @param message
     */
    private void sendIt(RequestContext ctx, TelegramLongPollingBot callback, MessageType message) {
        if ( message instanceof List ) {
            List msgs = (List)message;
            for ( int i = 0; i < msgs.size(); i++ ) {
                if (msgs.get(i) instanceof SendMessage) {
                    sendMsg(ctx, (SendMessage)msgs.get(i), callback);
                } else if (msgs.get(i) instanceof SendDocument) {
                    sendDoc(ctx, (SendDocument)msgs.get(i), callback);
                }
            }
        } else if (message instanceof SendMessage) {
            sendMsg(ctx, (SendMessage)message, callback);
        } else if (message instanceof SendDocument) {
            sendDoc(ctx, (SendDocument)message, callback);
        }
    }

    private void sendMsg(RequestContext ctx, SendMessage sendMessage, TelegramLongPollingBot callback ) {
        sendMessage.setChatId(ctx.getIncomeRequest().getMessage().getChatId());
        sendMessage.setReplyMarkup(getKeyboard());
        BotMessageUtil.sendMessage(sendMessage, callback);
    }

    private void sendDoc(RequestContext ctx, SendDocument sendDocument, TelegramLongPollingBot callback ) {
        sendDocument.setChatId(ctx.getIncomeRequest().getMessage().getChatId());
        sendDocument.setReplyMarkup(getKeyboard());
        BotMessageUtil.sendDocument(sendDocument, callback);
    }

    /**
     * This method generate message for command
     *
     * @param ctx to generate message for command
     * @return PartialBotApiMethod
     */
    protected abstract MessageType getMessage(RequestContext ctx);

    /**
     * Get command string representation
     *
     * @return String
     */
    protected abstract String getCommandText();

    /**
     * Render reply keyboard for client based on list of command which returned by getButtons method.
     *
     * @return ReplyKeyboard
     */
    private ReplyKeyboard getKeyboard() {
        if (this.buttons == null || this.buttons.isEmpty())
            return null;
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup();
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboard.getKeyboard().add(keyboardRow);
        for (int i = 0; i < buttons.size(); i++) {
            keyboardRow.add(new KeyboardButton(buttons.get(i).getCommandText()));
            if (i >= 1 && ((i + 1) % 2) == 0) {
                keyboardRow = new KeyboardRow();
                keyboard.getKeyboard().add(keyboardRow);
            }

        }
        if (askContact() && buttons.size() >= 2 && (buttons.size() % 2) == 0 ) {
            keyboardRow = new KeyboardRow();
            keyboardRow.add(new KeyboardButton().setText(BUNDLE.getString("YES")).setRequestContact(Boolean.TRUE));
            keyboard.getKeyboard().add(keyboardRow);
        } else if ( askContact() ) {
            keyboardRow.add(new KeyboardButton().setText(BUNDLE.getString("YES")).setRequestContact(Boolean.TRUE));
        }

        keyboard.setResizeKeyboard(Boolean.TRUE);
        keyboard.setOneTimeKeyboard(Boolean.TRUE);
        return keyboard;
    }

    /**
     * Register command as button to this command
     *
     * @param command to add
     */
    public void registerMe(AbstractCommand command) {
        this.buttons.add(command);
    }

    /**
     * Init command as needed
     */
    protected abstract void doInit();

    /**
     * Call doInit method
     */
    @Override
    public final void init() {

        if (!this.initiated) {
            logger.info(String.format("initializing process started on %s", this.getCommandText()));
            doInit();
            this.initiated = Boolean.TRUE;
            logger.info(String.format("initializing process on %s has done", this.getCommandText()));
        }
    }

    /**
     * If command need user input then it will be passed to this method
     * @param ctx
     * @param callback
     */
    public final void userInput(RequestContext ctx, TelegramLongPollingBot callback) {
        sendIt(ctx, callback, doUserInput(ctx));
    }

    /**
     * process user input then return appropriate message
     *
     * @param ctx to generate response
     * @return Generic Message Type
     */
    protected abstract MessageType doUserInput(RequestContext ctx);

    /**
     * Is contact need to request ?
     * override it and send True if you need user contact
     * @return
     */
    protected Boolean askContact() {
        return Boolean.FALSE;
    }

}
