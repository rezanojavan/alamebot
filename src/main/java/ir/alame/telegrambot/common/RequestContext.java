package ir.alame.telegrambot.common;

import org.telegram.telegrambots.api.objects.Update;

/**
 * Request Context class which hold data during command execution time.
 *
 * @author reza
 * @version 1.0
 * @since 13/08/2017
 */
public class RequestContext {

    /**
     * Income request which is sent by user
     */
    private Update incomeRequest;

    /**
     * Get income request
     * @return Update
     */
    public Update getIncomeRequest() {
        return incomeRequest;
    }

    /**
     * Set income request
     * @param incomeRequest to set
     */
    public void setIncomeRequest(Update incomeRequest) {
        this.incomeRequest = incomeRequest;
    }
}
