package ir.alame.telegrambot.common;

import ir.alame.telegrambot.utility.BotMessageUtil;
import ir.alame.telegrambot.utility.ErrorLoggerUtil;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Execute given command
 *
 * @author reza
 * @version 1.0
 * @since 13/08/2017
 */
public class CommandExecutor {

    /**
     * User input is needed by command
     */
    private static ConcurrentHashMap<String, AbstractCommand> needUserInput = new ConcurrentHashMap<>();

    /**
     * Execute received command
     * @param ctx to execute
     */
    public static void execute(RequestContext ctx, TelegramLongPollingBot callback ){
        String chatId = null;
        Boolean needToRemove = Boolean.TRUE;
        try {
            chatId = ctx.getIncomeRequest().getMessage().getChatId().toString();
            CommandFactory.getInstance().getCommand(ctx.getIncomeRequest().getMessage().getText()).run(ctx, callback);
        } catch (IllegalArgumentException e) {
            if ( needUserInput.containsKey(chatId)) {
                try {
                    needUserInput.get(chatId).userInput(ctx, callback);
                } catch (RuntimeException re) {
                    ErrorLoggerUtil.exception(re.getMessage(), re);
                    BotMessageUtil.sendMessage(BotMessageUtil.generateMessage(ctx, re.getMessage()).setChatId(chatId),
                            callback);
                } catch (Exception e1) {
                    needToRemove = Boolean.FALSE;
                    ErrorLoggerUtil.exception(e1.getMessage(), e1);
                    BotMessageUtil.sendMessage(BotMessageUtil.generateUnhamdledException(ctx), callback);
                }
            } else {
                ErrorLoggerUtil.exception(e.getMessage(), e);
                BotMessageUtil.sendMessage(BotMessageUtil.generateCommandNotFoundMsg(ctx), callback);
            }
        } catch ( Exception ex ) {
            ErrorLoggerUtil.exception(ex.getMessage(), ex);
            BotMessageUtil.sendMessage(BotMessageUtil.generateUnhamdledException(ctx), callback);
        } finally {
            //if ( needToRemove )
                //needUserInput.remove(chatId);
        }
    }

    /**
     * Get
     * @return
     */
    public static ConcurrentHashMap<String, AbstractCommand> getNeedUserInput() {
        return needUserInput;
    }

    /**
     * Set
     * @param needUserInput
     */
    public static void setNeedUserInput(ConcurrentHashMap<String, AbstractCommand> needUserInput) {
        CommandExecutor.needUserInput = needUserInput;
    }
}
