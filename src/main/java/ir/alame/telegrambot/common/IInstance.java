package ir.alame.telegrambot.common;

/**
 * An interface that force child to implement getInstance method
 *
 * @author reza
 * @version 1.0
 * @since 16/08/2017
 */
public interface IInstance {

    /**
     * Return instance of command
     * @return CommandInstance that is a generic type
     */
    Object getInstance();

}
