package ir.alame.telegrambot.common;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Command factory which create appropriate command executor
 *
 * @author reza
 * @version 1.0
 * @since 13/08/2017
 */
public class CommandFactory {

    /**
     * singleton instance of CommandFactory
     */
    private static volatile CommandFactory _instance = new CommandFactory();

    /**
     * Private constructor
     * It register commands
     */
    private CommandFactory() {

    }

    /**
     * Store commands class based on their command text. You can get command class by command text.
     */
    private ConcurrentHashMap<String, AbstractCommand> commands = new ConcurrentHashMap<String, AbstractCommand>();

    /**
     * Create new instance of command based on given param.
     * @param input to create new command
     * @return AbstractCommand
     * @throws IllegalArgumentException if appropriate class not found or parameter is null
     */
    public final ICommand getCommand(String input) {
        if ( input == null )
            throw new IllegalArgumentException("Input command text is null");
        AbstractCommand clazz = commands.get(input);
        if ( clazz == null )
            throw new IllegalArgumentException(String.format("No appropriate command found for input %s", input));
        try {
            return clazz;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get Singleton Instance
     * @return CommandFactory
     */
    public static CommandFactory getInstance() {
        return _instance;
    }

    /**
     * Register All command on factory
     * @param command
     */
    public void registerCommand(AbstractCommand command) {
        commands.put(command.getCommandText(), command);
    }
}
