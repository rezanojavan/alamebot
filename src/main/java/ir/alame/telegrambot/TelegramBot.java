package ir.alame.telegrambot;

import ir.alame.telegrambot.common.CommandExecutor;
import ir.alame.telegrambot.common.RequestContext;
import ir.alame.telegrambot.utility.AccessLoggerUtil;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

/**
 * Telegram Bot Implementation
 *
 * @author reza
 * @version 1.0
 * @since 28/06/2017
 */
public class TelegramBot extends TelegramLongPollingBot {


    /**
     * This method is called when receiving updates via GetUpdates method
     *
     * @param update Update received
     */
    @Override
    public void onUpdateReceived(Update update) {
        AccessLoggerUtil.info(String.format("Receive message : %s", update.getMessage().toString()));
        if (update.hasMessage()) {
            RequestContext context = new RequestContext();
            context.setIncomeRequest(update);
            CommandExecutor.execute(context, this);
        }
    }

    @Override
    public String getBotUsername() {
        return BotConfig.BOT_USERNAME;
    }

    @Override
    public String getBotToken() {
        return BotConfig.BOT_TOKEN;
    }
}