package ir.alame.telegrambot;

import ir.alame.telegrambot.common.AbstractCommand;
import ir.alame.telegrambot.common.CommandFactory;
import ir.alame.telegrambot.utility.ClassUtil;
import ir.alame.telegrambot.utility.ErrorLoggerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;


/**
 * Telegram Bot Main Class
 *
 * @author reza
 * @version 1.0
 * @since 28/06/2017
 */
@SpringBootApplication
public class TelegramBotApplication {

    /**
     * Console logger
     */
    private static final Logger logger = LoggerFactory.getLogger("TelegramBot");

    /**
     * The package name where commands implementation have been places.
     */
    public static final String BASE_COMMANDS_PACKAGE = "ir.alame.telegrambot.commands";

    /**
     * All commands register here to initial process start on them.
     */
    public static List<AbstractCommand> commands = new ArrayList<>();

    /**
     * This method automatically load classes in <code>ir.alame.telegrambot.commands</code> package.Only class will
     * be load which has <code>getInstance()</code> method. If class do not have <code>getInstance()</code> method you
     * should register it manualy by passing its instance to <code>registerCommand</code> method.
     * @return Package
     */
    public static Package loadCommandsPackage() {
        logger.info("Loading Commands is started");
        try {

            ClassUtil.loadAllClass(TelegramBotApplication.BASE_COMMANDS_PACKAGE);
            logger.info("Loading Commands finished");
        } catch (Exception e) {
            ErrorLoggerUtil.exception(String.format("Auto loading has failed. %s", e.getMessage()), e);
        }
        return null;
    }

    /**
     * Register new command on Bot
     * @param command to register
     */
    public static void registerCommand(AbstractCommand command) {
        TelegramBotApplication.commands.add(command);
    }

    /**
     * Initial all commands in commands list and register them in command factory instance
     */
    private static void initCommands() {
        logger.info("Command initializing process is started");
        if (commands.isEmpty() ) {
            logger.info("No command found so initializing command process has been terminated");
            return;
        }
        ArrayList<AbstractCommand> copy = new ArrayList<>();
        do{
            copy.clear();
            copy.addAll(commands);
            callInitOn(copy);
        } while (copy.size() != commands.size());
        logger.info("Command initializing process is done");
    }

    /**
     * Call init method on command
     * @param cmnds to call init
     */
    private static void callInitOn(ArrayList<AbstractCommand> cmnds) {
        for( AbstractCommand command : cmnds ) {
            command.init();
            CommandFactory.getInstance().registerCommand(command);
        }
    }

    /**
     * Program Main Method
     * @param args to pass
     */
    public static void main(String[] args) {
        try {
            SpringApplication.run(TelegramBotApplication.class, args);

            ApiContextInitializer.init();
            logger.info("Api Context initialized");
            TelegramBotsApi botsApi = new TelegramBotsApi();
            loadCommandsPackage();
            initCommands();
            botsApi.registerBot(new TelegramBot());
            logger.info("Telegram Bot is up and running now...");
        } catch (TelegramApiException e) {
            ErrorLoggerUtil.exception(String.format("Register bot on telegram servers has failed. %s", e.getMessage()), e);
        }

    }
}
