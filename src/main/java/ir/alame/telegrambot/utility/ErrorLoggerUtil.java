package ir.alame.telegrambot.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Error Log Utility
 *
 * @author reza
 * @version 1.0
 * @since 28/06/2017
 */
public class ErrorLoggerUtil {

    /**
     * Hold the name of error logger
     */
    public static final String ERROR_LOGGER_NAME = "error";

    /**
     * Logger that used to log message
     */
    private static final Logger logger = LoggerFactory.getLogger(ERROR_LOGGER_NAME);

    /**
     * Log to error file by INFO level
     * @param message to log
     * @param params to log
     */
    public static void info(String message, Object... params) {
        logger.info(message, params);
    }

    /**
     * Log to error file by DEBUG level
     * @param message to log
     * @param params to log
     */
    public static void debug(String message, Object... params) {
        logger.debug(message, params);
    }

    /**
     * Log to error file by DEBUG level
     * @param message to log
     * @param params to log
     */
    public static void error(String message, Object... params) {
        logger.error(message, params);
    }

    /**
     * Log to error file by DEBUG level
     * @param message to log
     * @param ex to log
     */
    public static void exception(String message, Throwable ex) {
        logger.error(message, ex);
    }
}
