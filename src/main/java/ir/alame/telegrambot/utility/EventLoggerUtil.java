package ir.alame.telegrambot.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Event Logging Utility
 *
 * @author reza
 * @version 1.0
 * @since 28/06/2017
 */
public class EventLoggerUtil {

    /**
     * Hold the name of event logger
     */
    public static final String EVENT_LOGGER_NAME = "event";

    /**
     * Logger that used to log message
     */
    private static final Logger logger = LoggerFactory.getLogger(EVENT_LOGGER_NAME);

    /**
     * Log to event file by INFO level
     * @param message to log
     * @param params to log
     */
    public static void info(String message, Object... params) {
        logger.info(message, params);
    }

    /**
     * Log to event file by DEBUG level
     * @param message to log
     * @param params to log
     */
    public static void debug(String message, Object... params) {
        logger.debug(message, params);
    }

    /**
     * Log to event file by DEBUG level
     * @param message to log
     * @param params to log
     */
    public static void error(String message, Object... params) {
        logger.error(message, params);
    }
}
