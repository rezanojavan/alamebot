package ir.alame.telegrambot.utility;

import ir.alame.telegrambot.common.RequestContext;
import org.telegram.telegrambots.api.methods.ParseMode;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.ResourceBundle;

/**
 * Bot Message Utility
 *
 * @author reza
 * @version 1.0
 * @since 14/08/2017
 */
public class BotMessageUtil {

    /**
     * Resource bundle contain messages
     */
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("bundle");

    /**
     * Error message when command not found
     */
    private static final String COMMAND_NOT_FOUND = "COMMAND_NOT_FOUND";

    /**
     * Error message when Unhandled exception occurred
     */
    private static final String UNHANDLED_EXCEPTION = "UNHANDLED_EXCEPTION";


    /**
     * Generate an error message when income command not found.
     *
     * @param ctx to create unhandled exception occurred
     * @return SendMessage
     */
    public static SendMessage generateCommandNotFoundMsg(RequestContext ctx) {
        return generateMessage(ctx, BUNDLE.getString(COMMAND_NOT_FOUND))
                .setChatId(ctx.getIncomeRequest().getMessage().getChatId())
                .setReplyToMessageId(ctx.getIncomeRequest().getMessage().getMessageId());
    }

    /**
     * Generate an error message when unha
     *
     * @param ctx to create message
     * @return SendMessage
     */
    public static SendMessage generateUnhamdledException(RequestContext ctx) {
        return generateMessage(ctx, BUNDLE.getString(UNHANDLED_EXCEPTION))
                .setChatId(ctx.getIncomeRequest().getMessage().getChatId())
                .setReplyToMessageId(ctx.getIncomeRequest().getMessage().getMessageId());
    }

    /**
     * Generate a message
     * @param ctx to generate message
     * @param messageText to generate message
     * @return SendMessage
     */
    public static SendMessage generateMessage(RequestContext ctx, String messageText) {
        return new SendMessage()
                .setParseMode(ParseMode.HTML)
                .setText(messageText);
    }

    /**
     * Send text message to client
     *
     * @param message should be send
     * @param callback where to send
     */
    public static void sendMessage(SendMessage message, TelegramLongPollingBot callback) {
        try {
            callback.sendMessage(message);
        } catch (TelegramApiException ex) {
            ErrorLoggerUtil.exception(String.format("Send response to client failed. cause : %s", ex.getMessage()),
                    ex);
        }
    }

    /**
     * Send document message to client
     *
     * @param message should be send
     * @param callback where to send
     */
    public static void sendDocument(SendDocument message, TelegramLongPollingBot callback) {
        try {
            callback.sendDocument(message);
        } catch (TelegramApiException ex) {
            ex.printStackTrace();
            ErrorLoggerUtil.exception(String.format("Send response to client failed. cause : %s", ex.getMessage()),
                    ex);
        }
    }
}
