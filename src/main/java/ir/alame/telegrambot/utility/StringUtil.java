package ir.alame.telegrambot.utility;

import java.io.*;
import java.util.*;

/**
 * String utility
 * @author reza
 * @version 1.0
 * @since 21/08/2017
 */
public class StringUtil {

    /**
     * Check given value to match by pattern
     * @param value to check
     * @param pattern to check
     * @return True if match otherwise False
     */
    public static Boolean isMatch(String value, String pattern) {
        if ( value == null )
            return Boolean.FALSE;
        return value.matches(pattern);
    }

    public static void main(String[] args) {
        print();
    }

    public static void print() {
        try {
            File file = new File("E:\\shoab.csv");
            FileReader fr = new FileReader(file);
            BufferedReader bf = new BufferedReader(fr);
            String line;
            Map<String,Set<String >> prov = new HashMap<>();
            while ((line = bf.readLine()) != null) {
                String[] fields = line.split(",");
                Set<String> s = prov.get(fields[3]);
                if ( s == null ) {
                    s = new TreeSet<>();
                    s.add(fields[4]);
                    prov.put(fields[3], s);
                } else {
                    s.add(fields[4]);
                }

            }
            System.out.println("xxx");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}