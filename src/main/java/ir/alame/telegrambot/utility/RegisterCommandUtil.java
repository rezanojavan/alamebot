package ir.alame.telegrambot.utility;

import ir.alame.telegrambot.common.AbstractCommand;
import ir.alame.telegrambot.common.CommandFactory;

/**
 * Register Command Utility
 * @author reza
 * @version 1.0
 * @since 17/08/2017
 */
public class RegisterCommandUtil {

    /**
     * This method register command on telegram bot
     * @param command to register
     */
    public static void registerCommand(AbstractCommand command) {
        //Main.registerCommand(command);
        CommandFactory.getInstance().registerCommand(command);
    }
}
