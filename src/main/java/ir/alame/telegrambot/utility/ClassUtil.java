package ir.alame.telegrambot.utility;

import ir.alame.telegrambot.common.AbstractCommand;
import org.reflections.Reflections;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

/**
 * Class utility
 * @author reza
 * @version 1.0
 * @since 16/08/2017
 */
public class ClassUtil {

    /**
     * Scans all classes accessible from the context class loader which belong to the given package and subpackages.
     *
     * @param packageName tp load class
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static void loadAllClass(String packageName) throws ClassNotFoundException, IOException {
        Reflections reflections = new Reflections(packageName);
        Set<Class<? extends AbstractCommand>> subTypes = reflections.getSubTypesOf(AbstractCommand.class);
        Iterator it = subTypes.iterator();
        while (it.hasNext())
            Class.forName(((Class) it.next()).getName());
    }
}
