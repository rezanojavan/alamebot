package ir.alame.telegrambot.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Access Logging Utility
 *
 * @author reza
 * @version 1.0
 * @since 14/08/2017
 */
public class AccessLoggerUtil {

    /**
     * Hold the name of access logger
     */
    public static final String ACCESS_LOGGER_NAME = "access";

    /**
     * Logger that used to log message
     */
    private static final Logger accessLogger = LoggerFactory.getLogger(ACCESS_LOGGER_NAME);

    /**
     * Log to access file by INFO level
     * @param message to log
     * @param params to log
     */
    public static void info(String message, Object... params) {
        accessLogger.info(message, params);
    }

    /**
     * Log to error file by DEBUG level
     * @param message to log
     * @param ex to log
     */
    public static void exception(String message, Throwable ex) {
        accessLogger.error(message, ex);
    }

}
